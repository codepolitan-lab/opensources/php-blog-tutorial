<?php
include('config.php');

$id = $_GET['id'];

$data = mysqli_query($con, 'SELECT * FROM posts WHERE id = '. $id);
$post = mysqli_fetch_assoc($data);

$judul = $post['judul'];
$subjudul = "";
$banner_url = "assets/img/post-bg.jpg";
?>
<?php include('header.php'); ?>

    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <?php echo nl2br($post['konten']); ?>
                </div>
            </div>
        </div>
    </article>

    <hr>

<?php include('footer.php'); ?>
<?php
session_start();

include('config.php');

if(! isset($_SESSION['loggedin']))
    exit('access forbidden');

$judul = "Form Tambah Artikel";
$subjudul = "";
$banner_url = "assets/img/home-bg.jpg";

if(isset($_POST['submit'])){
    $judul = $_POST['judul'];
    $konten = $_POST['konten'];
    $date_created = date("Y-m-d H:i:s");

    $query = mysqli_query($con, 'INSERT INTO posts (judul, konten, date_created) VALUES ("'.$judul.'", "'.$konten.'", "'.$date_created.'")');

    if($query){
        header('Location: index.php');
    } else {
        $error = "Error: " . mysqli_error();
    }
}
?>

<?php include('header.php'); ?>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <?php if(isset($error)): ?>
                <div class="alert alert-warning"><?php echo $error; ?></div>
            <?php endif;?>
        
            <form action="add_post.php" method="post">
                <label>Judul Artikel</label>
                <input type="text" name="judul" class="form-control">
                <br>
                <label>Konten Artikel</label>
                <textarea name="konten" class="form-control"></textarea>

                <br>
                <button type="submit" name="submit" value="submit">Submit Artikel</button>
            </form>

        </div>
    </div>

    <hr>

<?php include('footer.php'); ?>


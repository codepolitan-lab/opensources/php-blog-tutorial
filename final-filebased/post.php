<?php
session_start();
include('functions.php');

$id = $_GET['id'];
$file = $post_folder.'/'.$id.'.json';
if(!file_exists($file))
    die('post tidak ditemukan.');

$post = get_post_content($file);

// deskripsi halaman
$judul = $post['title'];
$subjudul = "";
$banner_url = "assets/img/post-bg.jpg";
?>
<?php include('shared/header.php'); ?>

    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <?php echo nl2br($post['content']); ?>
                </div>
            </div>
        </div>
    </article>

    <hr>

<?php include('shared/footer.php'); ?>
<?php
session_start();
include('functions.php');

if(! isset($_SESSION['loggedin']))
    exit('access forbidden');

if(isset($_POST['submit'])){
    $data['title'] = $_POST['title'];
    $data['content'] = $_POST['content'];
    $slug = time() .'-'. str_replace(" ", "-", $data['title']);

    if($result = write_post($slug, $data)){
        header('Location: edit_post.php?id='.$slug);
    } else {
        $error = "Error: " . $result;
    }
}

// deskripsi halaman
$judul = "Form Tambah Artikel";
$subjudul = "";
$banner_url = "assets/img/home-bg.jpg";
?>

<?php include('shared/header.php'); ?>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <?php if(isset($error)): ?>
                <div class="alert alert-warning"><?php echo $error; ?></div>
            <?php endif;?>
        
            <form action="add_post.php" method="post">
                <label>Judul Artikel</label>
                <input type="text" name="title" class="form-control">
                <br>
                <label>Konten Artikel</label>
                <textarea name="content" class="form-control" rows="20"></textarea>

                <br>
                <button type="submit" name="submit" value="submit">Simpan Artikel</button>
            </form>

        </div>
    </div>

    <hr>

<?php include('shared/footer.php'); ?>

